import firebase from "firebase";
import firestore from "firebase/firestore";

// Your web app's Firebase configuration
var firebaseConfig = {
  apiKey: "AIzaSyCAlztaPpDDuHkqUGt2yezNVn9Wih2dsGI",
  authDomain: "udemy-ninja-smoothies-a84d0.firebaseapp.com",
  databaseURL: "https://udemy-ninja-smoothies-a84d0.firebaseio.com",
  projectId: "udemy-ninja-smoothies-a84d0",
  storageBucket: "udemy-ninja-smoothies-a84d0.appspot.com",
  messagingSenderId: "147905494433",
  appId: "1:147905494433:web:ffa234c133385e43c55fe0",
  measurementId: "G-BVC2HNQ3F4"
};
// Initialize Firebase
const firebaseApp = firebase.initializeApp(firebaseConfig);
firebaseApp.firestore(); /*.settings({ timestampsInSnapshots: fa }); */
firebase.analytics();

// export firestore database
export default firebaseApp.firestore();
